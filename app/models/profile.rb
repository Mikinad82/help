class Profile < ActiveRecord::Base
	validates :user_id, presence: true, uniqueness: {message: "already created one."}
	validates :name, presence: true, length: { maximum: 50 }
	# The validation below is for uniqueness (no duplicates) on a model attribute.
	validates :initial, presence: true, length: { maximum: 1 }
	validates :phrase, presence: true
	validates :education, presence: true
	validates :certification, presence: true
	validates :experience, presence: true
	validates :rate, presence: true, length: { maximum: 3 }
	validates :zip, presence: true, length: { maximum: 5 }

    
	has_and_belongs_to_many :subjects
	belongs_to :user
    geocoded_by :zip
    after_validation :geocode, :if => :zip_changed?
    #reverse_geocoded_by :latitude, :longitude
    #fter_validation :reverse_geocode


	has_attached_file :avatar, styles: { small: "160x160#"}, :default_url => ActionController::Base.helpers.asset_path('missing.png')
    validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/

	

    searchable do
    	text :subject_search
  		latlon(:location) { Sunspot::Util::Coordinates.new(self.latitude, self.longitude) }
        string :search_near
    end

    
    def search_near
    	self.zip
    end

    def subject_search
    	subjects.map { |subject| subject.title }
    end


end
