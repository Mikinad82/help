class ProfilesController < ApplicationController
	before_action :authenticate_user!, only: [:new, :edit, :update, :destroy]
  respond_to :html, :json
 
    def index
      @search = Profile.search do 
        fulltext params[:subject_search]
        with(:location).in_radius(*Geocoder.coordinates(params[:search_near]), 10) if params[:search_near].present?
      end
      @profiles = @search.results
    end


    def new
    	@profile = Profile.new 
    end

    def show
      @profile = Profile.find(params[:id])
      profile = Profile.find(params[:id])
      @subject = profile.subjects
      
    end
      

    def create
      @profile = Profile.new(profile_params)
      @profile.user = current_user
      if @profile.save
        @profile.subjects << subject
      	flash[:success] = "Profile successfully created."
      	redirect_to @profile
      else
      	render 'new'
      end
    end

    def edit
      @profile = Profile.find(params[:id])
    end

    def update
      @profile = Profile.find(params[:id])
      if @profile.update_attributes(profile_params)
        flash[:success] = "Profile updated"
        redirect_to @profile
      else
        render 'edit'
      end
    end


  

    private

      
         def subject
          @subject = Subject.where("id = ?", params[subject_ids: []])
         end
      

         def profile_params
      	  params.require(:profile).permit(:avatar, :name, :initial, :phrase, :education, :certification, :experience, :rate, :zip, :latitude, :longitude, subject_ids: [])
         end

end
 