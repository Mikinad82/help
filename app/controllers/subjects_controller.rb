class SubjectsController < ApplicationController
	respond_to :html, :json
	

 def index
    @subject = Subject.all
    respond_with @subject
  end

  def new
  	@subject = Subject.new 
  end

  def show
    @subject = Subject.find(params[:id])
  end

  def create
  @subject = Subject.new(subject_params)
    if @subject.save
    	flash[:success] = "Subject successfully created."
    	redirect_to @subject
    else
    	render 'new'
    end
  end

   private

      

      def subject_params
    	  params.require(:subject).permit(:heading, :title)
      end

end
