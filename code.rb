if params[:search].present?
@search = Restaurant.solr_search do
fulltext params[:restaurant_name] # runs a full text search of
with(:approved, :true) #facets approved restaurants
if params[:cuisine_search].present? #user also entered cuisine preference
any_of do
params[:cuisine_search].each do |tag|
with(:cuisines_name, tag) # facet by matching cuisines
end
end
end
if params[:address].present? || params[:city_search].present? || params[:state_search].present? || params[:zip_search].present?
#if any location fields are present, geocode that location
with(:location).in_radius(*Geocoder.coordinates(whereat), howfar)
#facet based on user given location,
end
order_by_geodist(:location,request.location.latitude,request.location.longitude)
@restaurants = @search.results
end