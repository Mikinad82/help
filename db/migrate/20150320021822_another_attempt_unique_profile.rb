class AnotherAttemptUniqueProfile < ActiveRecord::Migration
  def change
  	remove_reference :profiles, :user, index: true, unique: true
    remove_foreign_key :profiles, :users
  end
end
