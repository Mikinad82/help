class AddLatitudeAndLongitueToProfile < ActiveRecord::Migration
  def change
    add_column :profiles, :latitude, :float
    add_column :profiles, :longitude, :float
    add_column :profiles, :zip, :string
  end
end
