class FixIndexesForUserId < ActiveRecord::Migration
  def change
  	remove_reference :profiles, :user, index: true
    remove_foreign_key :profiles, :users
    add_reference :profiles, :user, index: true, unique: true
    add_foreign_key :profiles, :users
  end
end
