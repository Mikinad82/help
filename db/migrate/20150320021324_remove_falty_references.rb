class RemoveFaltyReferences < ActiveRecord::Migration
  def change
  	  remove_reference :profiles, :user, index: true
      remove_foreign_key :profiles, :users
  end
end
