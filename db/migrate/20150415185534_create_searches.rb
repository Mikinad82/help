class CreateSearches < ActiveRecord::Migration
  def change
    create_table :searches do |t|
      t.string :subject
      t.string :zip

      t.timestamps null: false
    end
  end
end
