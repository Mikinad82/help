class LastOneUserIdProfile < ActiveRecord::Migration
  def change
  	add_reference :profiles, :user, index: true
    add_foreign_key :profiles, :users, unique: true
  end
end
