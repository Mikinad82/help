class CreateProfilesSubjects < ActiveRecord::Migration
  def change
    create_table :profiles_subjects, :id => false do |t|
      t.integer :profile_id
      t.integer :subject_id
    end
  end
end
