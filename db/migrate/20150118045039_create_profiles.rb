class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.string :name
      t.string :initial
      t.text   :phrase
      t.string :education
      t.text   :certification
      t.text   :subject
      t.text   :experience
      t.string :rate

      t.timestamps null: false
    end
  end
end
