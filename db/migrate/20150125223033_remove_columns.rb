class RemoveColumns < ActiveRecord::Migration
  def up
  	remove_column :profiles, :subject
  end

  def down
    add_column :profiles, :subject, :text
  end

end
