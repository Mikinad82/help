Rails.application.routes.draw do
  
  root 'home#index'
  get 'subjects/index'
  

  devise_for :users
  resources :dashboard
  resources :subjects
  resources :profiles
  	
end
